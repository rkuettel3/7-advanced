FROM busybox

# Values for build-time variables might be supplied using the --build-arg flag.
# See: https://docs.docker.com/engine/reference/commandline/build/#build-arg.
ARG Version=1.0
ARG Author=UNKNOWN

RUN echo "#!/bin/sh\n" > /version.sh
RUN echo -e "echo \"\nThis is Version ${Version} of this image.\"" >> /version.sh
RUN echo -e "echo \"It was build by ${Author}.\n\"" >> /version.sh

ENTRYPOINT [ "sh", "/version.sh"]