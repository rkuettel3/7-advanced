# 7 Advanced

## 2 Signaturen überprüfen

Mit `cosign-gitlab-gipe.pub`:

```bash
# 1
cosign verify --key cosign-gitlab-gipe.pub registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:1.0
# Error: no matching signatures
# main.go:69: error during command execution: no matching signatures

# 2
cosign verify --key cosign-gitlab-gipe.pub registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:2.0
#Verification for registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:2.0 --
# The following checks were performed on each of these signatures:
#  - The cosign claims were validated
#  - Existence of the claims in the transparency log was verified offline
#  - The signatures were verified against the specified public key
#
# ...

# 3
cosign verify --key cosign-gitlab-gipe.pub registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:3.0
# Error: no matching signatures: error verifying bundle: comparing public key PEMs, expected -----BEGIN PUBLIC KEY-----
# MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE0DwKBay+URskuTiXwHeo37q9zhL6
# q6cg5jDytofhZvjJxZLmA8/S1MgLqUjN2rHZxOIEaKLvTk1R6YwAP9IWrw==
# -----END PUBLIC KEY-----
# , got -----BEGIN PUBLIC KEY-----
# MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEKZBwyDgRFfZvdsLxmw752dVtxFSI
# Ss1lnZA8Y5l6umPET93wVYtrgMiibmj4EiOVrFPCQRy8gWJLDCdmSaZU7A==
# -----END PUBLIC KEY-----

# main.go:69: error during command execution: no matching signatures: error verifying bundle: comparing public key PEMs, expected -----BEGIN PUBLIC KEY-----
# MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE0DwKBay+URskuTiXwHeo37q9zhL6
# q6cg5jDytofhZvjJxZLmA8/S1MgLqUjN2rHZxOIEaKLvTk1R6YwAP9IWrw==
# -----END PUBLIC KEY-----
# , got 
# -----BEGIN PUBLIC KEY-----
# MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEKZBwyDgRFfZvdsLxmw752dVtxFSI
# Ss1lnZA8Y5l6umPET93wVYtrgMiibmj4EiOVrFPCQRy8gWJLDCdmSaZU7A==
# -----END PUBLIC KEY-----
```

Mit `gitlab://43621110`:

```bash
# 1
GITLAB_TOKEN="glpat-NJzDrpBxiZySSYyw3oy_" cosign verify --key gitlab://43621110 registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:1.0
# Error: no matching signatures
# main.go:69: error during command execution: no matching signatures

# 2
GITLAB_TOKEN="glpat-NJzDrpBxiZySSYyw3oy_" cosign verify --key gitlab://43621110 registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:2.0
# Verification for registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:2.0 --
# The following checks were performed on each of these signatures:
#   - The cosign claims were validated
#   - Existence of the claims in the transparency log was verified offline
#   - The signatures were verified against the specified public key
# 
# ...

# 3
GITLAB_TOKEN="glpat-NJzDrpBxiZySSYyw3oy_" cosign verify --key gitlab://43621110 registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:3.0
# Verification for registry.gitlab.com/gibz/teaching/m347/sample-applications/simple-dockerfile:3.0 --
# The following checks were performed on each of these signatures:
#   - The cosign claims were validated
#   - Existence of the claims in the transparency log was verified offline
#   - The signatures were verified against the specified public key
# 
# ...
```

## Interpretation

### 1

Keine Übereinstimmende Signaturen wurden gefunden.

### 2

Die Signatur stimmt überein.
Dies wurde durch folgender prozesse überprüft:

- Claims konnten validiert werden
- Claims wurden offline vertifiziert
- Die signaturen konten vertifiziert werden

### 3

#### Mit `cosign-gitlab-gipe.pub`:
Die Signaturen stimmten nicht überein. Darauf wird angezeit welcher erwartet wird und welcher bekommen wurde.

#### Mit `gitlab://43621110`:
Die Signatur stimmt überein.
Dies wurde durch folgender prozesse überprüft:

- Claims konnten validiert werden
- Claims wurden offline vertifiziert
- Die signaturen konten vertifiziert werden


# 3 eigene Images signieren

## Schlüsselpaar generieren

```bash
GITLAB_TOKEN="<gitlab token>" cosign generate-key-pair gitlab://55754874
```

## Publizieren des Images

```bash
#build
docker build -t registry.gitlab.com/rkuettel3/7-advanced:1.0 .

#push
docker push registry.gitlab.com/rkuettel3/7-advanced:1.0
```

## Signieren eines images

```bash
GITLAB_TOKEN="<gitlab token>" cosign sign --key gitlab://55754874 registry.gitlab.com/rkuettel3/7-advanced:1.0
```

## Vertifizieren eines Images

```bash
cosign verify --key ./cosign.pub registry.gitlab.com/rkuettel3/7-advanced:1.0
```